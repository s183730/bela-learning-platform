### Bela DSP Box ###

<img src="./main_projects/documentation/bela_prototype_text.jpg" alt="Bela DSP Box layout" width="500"/>

A real-time audio DSP platform based on the Bela Platform used as a tool for learning in the 
DTU course 22051 'Signals and Linear Systems in Discrete Time'.

To use this code without the Bela DSP Box, create a project on your Bela Platform and upload the files to the project.

The main projects in this repository are documented and can be found on the [Bela DSP Box documentaition website](https://www.student.dtu.dk/~s183730/bela-learning-platform/main_projects/documentation/html/)

2021 Nikolai Linden-Vørnle