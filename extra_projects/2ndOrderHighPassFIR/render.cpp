/*
	FIR 2nd order High-pass filter
	------------------------------
	
	This program implementes a simple second order FIR highpass filter. This is done with
	the difference equation y[n] = 1*x[n-2] - 1.6*x[n-1] + x[n]. The coefficients are
	calculated to give two zeros at 0.8*exp(i*0) in the z-plane.
	
	(c) Nikolai Linden-Vørnle
*/
#include <Bela.h>

// initialize two variables to store the delayed samples
float z1 = 0;
float z2 = 0;

bool setup(BelaContext *context, void *userData)
{
	return true;
}

void render(BelaContext *context, void *userData)
{
	for (int n = 0; n < context->audioFrames; n++) {
		float x = 0;	// input x[n] for current frame
		float y = 0;	// output y[n] for current frame
		
		// read samples from each channel in the input buffer
		for (int ch = 0; ch < context->audioInChannels; ch++) {
			x += audioRead(context, n, ch);
		}
		// normalize with number of input channels
		x = x / context->audioInChannels;
		
		// calculate the output sample from the difference equation
		y = 1*z2 -1.6*z1 + 0.64*x;
		
		// update the delayed samples
		z2 = z1;
		z1 = x;
		
		// write the output sample to the output buffers
		for (int ch = 0; ch < context->audioOutChannels; ch++) {
			audioWrite(context, n, ch, y);
		}
	}
}

void cleanup(BelaContext *context, void *userData)
{
}