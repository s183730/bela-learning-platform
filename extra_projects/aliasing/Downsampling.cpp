/***** Downsampling.cpp *****/
#include "Downsampling.h"

Downsampling::Downsampling() {
}

Downsampling::Downsampling(int samplingFactor) {
	Downsampling::setup(samplingFactor);
}

Downsampling::~Downsampling() {
}
	
void Downsampling::setup(int samplingFactor) {
	state_ = 0;
	samplingFactor_ = samplingFactor;
}

void Downsampling::setBypass(bool bypass){
	bypass_ = bypass;
}

bool Downsampling::getBypass() {
	return bypass_;
}

void Downsampling::setSamplingFactor(int dsFacter) {
	samplingFactor_ = dsFacter;
}

int Downsampling::getSamplingFactor() {
	return samplingFactor_;
};