/*
	Aliasing
	--------
	
	This project is used to show the concept of aliasing. 
	This is done by downsampling the input audio, where only a fraction
	of the input samples a written to the output buffers, effectivly reducing 
	the sampling frequency of the samples.
	
	(c) Nikolai Linden-Vørnle
*/

#include <Bela.h>
#include "Downsampling.h"
#include <vector>

// the amount of downsampling
const int kDownsampleFactor = 5;

std::vector<Downsampling> gDownSampling;

bool setup(BelaContext *context, void *userData)
{
	// initialize the downsampling objects
	gDownSampling.resize(context->audioInChannels);
	
	for (int i = 0; i < gDownSampling.size(); i++) {
		gDownSampling[i].setup(kDownsampleFactor);
	}
	
	// checks if the number of audio in/out channels match
	if (context->audioInChannels != context->audioOutChannels) {
		return false;
	}
	
	return true;
}

void render(BelaContext *context, void *userData)
{
	for (int n = 0; n < context->audioFrames; n++) {
		for (int ch = 0; ch < context->audioInChannels; ch++) {
			
			// the input sample is processed through the downsampling object
			// and written to the output buffer. Notice that the downsampling
			// object doesn't have anti-aliasing. This is to show the effect of aliasing
			
			float out = gDownSampling[ch].process(audioRead(context, n, ch));
			audioWrite(context, n, ch, out);
		}
	}
}

void cleanup(BelaContext *context, void *userData)
{
}