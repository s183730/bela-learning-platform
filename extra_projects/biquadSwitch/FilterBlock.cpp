/***** filterBlock.cpp *****/

#include "FilterBlock.h"

FilterBlock::FilterBlock() {
}

FilterBlock::~FilterBlock() {
}

int FilterBlock::setup(double fs, double cutoff, double q) {
	filters_.resize(4);
	
	Biquad::Settings settings {
		.fs = fs,
		.type = Biquad::lowpass,
		.cutoff = cutoff,
		.q = q,
		.peakGainDb = 0.0
	};
	
	for (int i = 0; i < 4; i++) {
		filters_[i].setup(settings);
	}
	
	// initialize member states.
	state_ = FilterBlock::lowpass;
	order_ = FilterBlock::second;
	
	return 0;	
}

FilterBlock::FilterState FilterBlock::getState() {
	return state_;
}

FilterBlock::FilterOrder FilterBlock::getOrder() {
	return order_;
}

void FilterBlock::switchState() {
	// switch to the next type of filter if function is called
	if (state_ == lowpass) {
		for (int i = 0; i < 4; i++) {
			filters_[i].setType(Biquad::highpass);
		}
		state_ = highpass;
	} else if (state_ == highpass) {
		for (int i = 0; i < 4; i++) {
			filters_[i].setType(Biquad::bandpass);
		}
		state_ = bandpass;
	} else if (state_ == bandpass) {
		for (int i = 0; i < 4; i++) {
			filters_[i].setType(Biquad::notch);
		}
		state_ = bandstop;
	} else {
		for (int i = 0; i < 4; i++) {
			filters_[i].setType(Biquad::lowpass);
		}
		state_ = lowpass;
	}
}

void FilterBlock::switchOrder() {
	// cycle through filter orders
	if (order_ == second) {
		order_ = fourth;
	} else if (order_ == fourth) {
		order_ = sixth;
	} else if (order_ == sixth) {
		order_ = eighth;
	} else {
		order_ = second;
	}
}

void FilterBlock::setState(FilterBlock::FilterState state) {
	state_ = state;
}

void FilterBlock::setOrder(FilterBlock::FilterOrder order) {
	order_ = order;
}

void FilterBlock::resetBiquad() {
	for (int i = 0; i < 4; i++) {
		filters_[i].clean();
	}
}

void FilterBlock::setQ(double q) {
	for (int i = 0; i < 4; i++) {
		filters_[i].setQ(q);
	}
}

void FilterBlock::setFc(double fc) {
	for (int i = 0; i < 4; i++) {
		filters_[i].setFc(fc);
	}
}