/***** filterBlock.h *****/

#pragma once
#include <libraries/Biquad/Biquad.h>
#include <vector>

class FilterBlock {
	public:
		typedef enum {
			second = 0,
			fourth, 
			sixth,
			eighth
		} FilterOrder;
		typedef enum {
			lowpass = 0,
			highpass,
			bandpass,
			bandstop
		} FilterState;
		
		// constructors/deconstructors
		FilterBlock();
		~FilterBlock();
		
		// setup function. initialize biquad objects and members
		int setup(double fs, double cutoff, double q);
		
		// process sample an return output. update member states
		float process(float in);
		
		FilterState getState();
		FilterOrder getOrder();
		
		// cycle through filter states/order
		void switchState();
		void switchOrder();
		
		// set filter state/order explicitly
		void setState(FilterBlock::FilterState state);
		void setOrder(FilterBlock::FilterOrder order);
		
		// change settings for the Biquad objects 
		void resetBiquad();
		void setQ(double q);
		void setFc(double fc);
	
	protected:
		FilterState state_;
		FilterOrder order_;
		std::vector<Biquad> filters_;
};

inline float FilterBlock::process(float in) {
	float out = 0.0;
	
	out = filters_[0].process(in);
	
	for (int i = 1; i <= order_; i++) {
		out = filters_[i].process(out);
	}
	
	return out;
}