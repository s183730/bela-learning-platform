/*
	State-variable filter of different orders
	-----------------------------------------
	
	This program makes the user able to switch between lowpass, highpass, bandpass
	and bandstop filters of 2nd, 4th, 6th and 8th order. Every time the user presses
	one of the "state" or "order" buttons, the state of the filter or order of the filter
	are changed, cycling through the different modes. The system starts as a 2nd order
	lowpass filter.
	
	LED's are used to indicate the state and order of the filter. The settings are:
	
	STATE LED:
	not lit = lowpass
	1/3 lit = highpass
	2/3 lit = bandpass
	fully lit = bandstop
	
	ORDER LED:
	not lit = 2nd order
	1/3 lit = 4th order
	2/3 lit = 6th order
	fully lit = 8th order
	
	A potentiometer is used to control the cutoff frequency of the filter.
	the constants "kFcMIN" and "kFcMAX" are used to set the minimum and 
	maximum cutoff frequency for the filter respectivly.
	
	I/O CONNECTIONS
	-----------------------------------------
	One of the legs of the button is connected to one of the digital I/O pins. 
	The same leg is connected to the Bela 3.3V supply through a 10kOhm resistor.
	The second leg of the button is connected to the Bela's ground pin.
	
	LED's are connected to the analog outputs to show the state or order of the 
	filter, through a 1kOhm resistor to ground, making sure that a safe amount 
	of current flows in the LED's (220Ohm for some LED's, which are less bright). 
	
	A potentiometer (10kOhm) is used to adjust the cutoff frequency of the filter. 
	The left-most leg is connected to ground, the middel leg is connected to one 
	of the "analog inputs" and the right-most leg is connected to the Bela's 3.3V 
	supply. 
	-----------------------------------------
	
	The 'Biquad' library is used to create the Biquad structures, implementing
	the filters. The audio is processed in stereo.
	
	Author: Nikolai Linden-Vørnle, 2021
*/
#include <Bela.h>
#include "FilterBlock.h"
#include <libraries/Debounce/BelaDebounce.h>
#include <vector>

// stereo pair of filter blocks
std::vector<FilterBlock> filters(2);

// default values for the filters
float gCutoff = 2000.0; 	// cutoff frequency for the filter objects (in Hz)
float gQ = 2.0;				// Q factor for filter objects

// constants
const double kDebounceMs = 100.0;
const float kFcMIN = 200.0;				// minimum cutoff frequency potentiometer
const float kFcMAX = 5000.0;			// maximum cutoff frequency from potentiometer
const unsigned int kStateButton = 0;	// digital pin for state button
const unsigned int kOrderButten = 1;	// digital pin for order button
const unsigned int kLEDstate = 0;		// analog output pin for order LED
const unsigned int kLEDorder = 1;		// analog output pin for state LED
const unsigned int kPotFreq = 0;		// analog input pin for frequency potentiometer

// global variable used to for edge detection on buttons
int gLastReadingStateButton = 1;
int gLastReadingOrderButton = 1;

// debounce objets for digital inputs
BelaDebounce gDebounceStateButton;
BelaDebounce gDebounceOrderButton;

bool setup(BelaContext *context, void *userData)
{
	// checks if the Bela system has stereo audio I/O
	if (context->audioInChannels != 2 && context->audioOutChannels != 2) {
		return false;
	}
	
	// initialize filter block objects
	for (unsigned int i = 0; i < filters.size(); i++) {
		filters[i].setup(context->audioSampleRate, gCutoff, gQ);
	}
	
	// checks if the digital I/O pins are enabled
	if (context->digitalChannels == 0) {
		rt_printf("No digital I/O pins are enabled!!");
		return false;
	}
	
	// checks if the analog inputs are enabled
	if (context->analogInChannels == 0) {
		rt_printf("No analog inputs are enabled!!");
		return false;
	}
	
	// initialize digital pins as inputs
	BelaDebounce::Settings settings {
		.context = context,
		.channel = kStateButton,
		.debounceMs = kDebounceMs
	};
	gDebounceStateButton.setup(settings);
	settings.channel = kOrderButten;
	gDebounceOrderButton.setup(settings);
	
	return true;
}

void render(BelaContext *context, void *userData)
{
	int currentReadingStateButton = (int)gDebounceStateButton.process(context);
	int currentReadingOrderButton = (int)gDebounceOrderButton.process(context);
	
	// checks if the 'state' button has been pressed and change filter state
	if (currentReadingStateButton == 0 && gLastReadingStateButton == 1) {
		for (unsigned int i = 0; i < filters.size(); i++) {
			filters[i].switchState();
		}
	}
	
	// checks if the 'order' button has been pressed and change the order
	if (currentReadingOrderButton == 0 && gLastReadingOrderButton == 1) {
		for (unsigned int i = 0; i < filters.size(); i++) {
			filters[i].switchOrder();
		}
	}
	
	// store current state for comparison in next block
	gLastReadingStateButton = currentReadingStateButton;
	gLastReadingOrderButton = currentReadingOrderButton;
	
	// set the light intensity on the LED to show the filter state
	FilterBlock::FilterState state = filters[0].getState();
	
	if (state == FilterBlock::lowpass) {
		analogWrite(context, 0, kLEDstate, 0.2); 
	} else if (state == FilterBlock::highpass) {
		analogWrite(context, 0, kLEDstate, 0.4);
	} else if (state == FilterBlock::bandpass) {
		analogWrite(context, 0, kLEDstate, 0.6);
	} else if (state == FilterBlock::bandstop) {
		analogWrite(context, 0, kLEDstate, 0.8);
	} else {
		analogWrite(context, 0, kLEDstate, 0.0);
	}
	
	// set the light intensity on the LED to show the filter order
	FilterBlock::FilterOrder order = filters[0].getOrder();
	
	if (order == FilterBlock::second) {
		analogWrite(context, 0, kLEDorder, 0.2);
	} else if (order == FilterBlock::fourth) {
		analogWrite(context, 0, kLEDorder, 0.4);
	} else if (order == FilterBlock::sixth) {
		analogWrite(context, 0, kLEDorder, 0.6);
	} else if (order == FilterBlock::eighth) {
		analogWrite(context, 0, kLEDorder, 0.8);
	} else {
		analogWrite(context, 0, kLEDorder, 0.0);
	}
	
	// read an analog voltage from a potentiometer and set the cutoff frequency of the filters
	float potFreq = map(analogRead(context, 0, kPotFreq), 0.0, 3.3 / 4.096, kFcMIN, kFcMAX);
	
	// set the new cutoff frequency in the filter obejcts
	for (unsigned int i = 0; i < filters.size(); i++) {
		filters[i].setFc(potFreq);
	}
	
	// process audio frames through "FilterBlock" obejcts
	for (unsigned int n = 0; n < context->audioFrames; n++) {
		float in = 0, out = 0;
		
		for (unsigned int ch = 0; ch < context->audioInChannels; ch++) {
			in = audioRead(context, n, ch);
			out = filters[ch].process(in);
			audioWrite(context, n, ch, out);
		}
	}
}

void cleanup(BelaContext *context, void *userData)
{
}