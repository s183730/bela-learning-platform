/***** DelayDAFX.cpp *****/
#include "DelayDAFX.h"

DelayDAFX::DelayDAFX() {
}

DelayDAFX::~DelayDAFX(){
}

void DelayDAFX::setup(const DelayDAFX::Settings &settings) {
	fs_ = settings.fs;
	
	maxDelaySamples_ = int (settings.maxDelayTime * fs_);
	if (maxDelaySamples_ <= 0) {
		maxDelaySamples_ = 1;
	}
	
	setDelayTime(settings.delayTime);
	
	setMix(settings.mix);
	
	setFeedback(settings.feedback);
	
	readPointer_ = 0;
	writePointer_ = 0;
	delayBuffer_.resize(maxDelaySamples_, 0.0f);
}

void DelayDAFX::setDelayTime(float delayTime) {
	delaySamples_ = int (delayTime * fs_);
	if (delaySamples_ > maxDelaySamples_) {
		delaySamples_ = maxDelaySamples_;
	}
	if (delaySamples_ < 0) {
		delaySamples_ = 0;
	}
}

void DelayDAFX::setMix(float mix) {
	mix_ = mix;
	if (mix_ > 1.0f) {
		mix_ = 1.0f;
	}
	if (mix_ < 0.0f) {
		mix_ = 0.0f;
	}
}	

void DelayDAFX::setFeedback(float feedback) {
	feedback_ = feedback;
	if (feedback_ >= 1.0f) {
		feedback_ = 0.99f;
	}
	if (feedback_ < 0.0f) {
		feedback_ = 0.0f;
	}
}

float DelayDAFX::getDelayTime() {
	return delaySamples_ / fs_;
}

float DelayDAFX::getMix() {
	return mix_;
}

float DelayDAFX::getFeedback() {
	return feedback_;
}

void DelayDAFX::resetDelay() {
	readPointer_ = 0;
	writePointer_ = 0;
	for (int i = 0; i < delayBuffer_.size(); i++) {
		delayBuffer_[i] = 0.0f;
	}
}