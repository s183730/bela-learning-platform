/*
	Delay DAFX with feedback
	------------------------
	Delay effect using a circular buffer, inspired by the Bela lectures by Andrew McPherson.
	
	The class "DelayDAFX" is used to generate an 'echo' or delay Digital Audio Effect.
	the DelayDAFX::Settings strucct is used to initialize the "DelayDAFX" object.
	
	In the render() function, .process() needs to be called on each sample/frame of audio.
	This will return the output sample, which is stored in the audio output buffer.
	
	Author: Nikolai Linden-Vørnle 2021
*/

#include <Bela.h>
#include <vector>
#include "DelayDAFX.h"
#include <libraries/OnePole/OnePole.h>

// delay object
std::vector<DelayDAFX> gDelay;

// constants
const unsigned int kPot0 = 0;
const unsigned int kPot1 = 1;
const unsigned int kPot2 = 2;

// input filer objects
std::vector<OnePole> inputFilters;

bool setup(BelaContext *context, void *userData)
{
	// initialize input potentiometer filters
	inputFilters.resize(3);
	for (unsigned int i = 0; i < inputFilters.size(); i++) {
		inputFilters[i].setup(30.0f, context->audioSampleRate, OnePole::LOWPASS);
	}
	
	DelayDAFX::Settings settings {
		.maxDelayTime = 0.5,
		.delayTime = 0.100,
		.mix = 0.2,
		.feedback = 0.3,
		.fs = context->audioSampleRate,
	};
	
	// resize the delay vector to contain two delay objects, one for each stereo channel
	gDelay.resize(2);
	
	// using the Settings struct, the delay object is initialized.
	for (unsigned int ch = 0; ch < gDelay.size(); ch++) {
		gDelay[ch].setup(settings);
	}
	
	return true;
}

void render(BelaContext *context, void *userData)
{
	// Update delay parameters
	float delayMix = map(analogRead(context, 0, kPot0), 0.0f, 3.3f/4.096f, 0.0f, 1.0f);
	delayMix = inputFilters[0].process(delayMix);
	if (gDelay[0].getMix() != delayMix) {
		for (unsigned int ch = 0; ch < gDelay.size(); ch++) {
			gDelay[ch].setMix(delayMix);
		}
	}
	
	float delayTime = map(analogRead(context, 0, kPot1), 0.0f, 3.3f/4.096f, 0.0f, 0.5f);
	delayTime = inputFilters[1].process(delayTime);
	if (gDelay[0].getDelayTime() != delayTime) {
		for (unsigned int ch = 0; ch < gDelay.size(); ch++) {
			gDelay[ch].setDelayTime(delayTime);
		}
	}
	
	float delayFeedback = map(analogRead(context, 0, kPot2), 0.0f, 3.3f/4.096f, 0.0f, 0.95f);
	delayFeedback = inputFilters[2].process(delayFeedback);
	if (gDelay[0].getFeedback() != delayFeedback) {
		for (unsigned int ch = 0; ch < gDelay.size(); ch++) {
			gDelay[ch].setFeedback(delayFeedback);
		}
	}
		
	for (unsigned int n = 0; n < context->audioFrames; n++) {
		for (unsigned int ch = 0; ch < gDelay.size(); ch++) {
			audioWrite(context, n, ch, gDelay[ch].process(audioRead(context, n, ch)));
		}
		
	}
}

void cleanup(BelaContext *context, void *userData)
{
}