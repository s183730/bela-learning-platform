/***** FlangerDAFX.h *****/
#pragma once
#include <vector>
#include <cmath>
#include <libraries/math_neon/math_neon.h>

/**
 * \brief Flanger DAFX
 * 
 *	A class used to implemented a flanger DAFX. The modulation width and frequency can be
 *	controlled in the render() in real-time, aswell as mixing between dry and wet signals. 
 *	Feedback control is also available, making it possible to create more "intense" flanging effects.
 */
class FlangerDAFX {
	public:
		struct Settings {
			float width;	///< Width of the delay line modulation. Values between 0-3ms are typical values (S. Disch 1999) 
			float modFreq;	///< Frequency of the modulation sinusoid. Typical values are between 0.1-5Hz (S. Disch 1999)
			float mix;		///< Mix between dry (unaffected) and wet (delayed/modulated) signals. Values between 0.0 and 1.0 are valid.
			float feedback;	///< Feedback coefficient. Values between 0.0 and <1.0 are valid.
			float fs;		///< Sampling frequency of the Bela.
		};
		
		/**
		 *	Constructor.
		 */
		 FlangerDAFX();
		 
		/**
		 *	Deconstructor.
		 */
		~FlangerDAFX();
		
		/**
		 *	Process on frame through the flanger object and return output frame.
		 */
		float process(float in);
		
		/**
		 *	Used to initialize the flanger object by passing #Settings struct with parameters.
		 */
		void setup(Settings &settings);
		
		/**
		 *	Set the current modulation width of the flanger. If the width is larger than
		 *	#MAXWIDTHSAMPLES_ the width will be set to #MAXWIDTHSAMPLES_
		 */
		void setWidth(float width);
		
		/**
		 *	Sets the current frequency of the modulation sinusoid. Typical ranges are 0.1-5Hz (S. Disch 1999)
		 */
		void setModFreq(float modFreq);
		
		/**
		 *	Sets the current mix between the dry (unaffected) and the wet (delayed) signals.
		 *	Input values between 0.0 and 1.0 are valid values. 
		 */
		void setMix(float mix);
		
		/**
		 *	Sets the amount of feedback in the flanger. 
		 *	Input values between 0.0 and <1.0 are valid values.
		 */
		void setFeedback(float feedback);
		
		/**
		 *	Returns the current modulation width in the flanger (in seconds).
		 */
		float getWidth();
		
		/**
		 *	Returns the current modulation frequency of the flanger (in Hertz).
		 */
		float getModFreq();
		
		/**
		 *	Returns the current dry/wet mix value of the flanger.
		 */
		float getMix();
		
		/**
		 *	Returns the current feedback value of the flanger.
		 */
		float getFeedback();
		
	protected:
		std::vector<float> delayLine_;
		int writePointer_;
		float readPointer_;
		
		float mix_;
		float blend_;
		float feedforward_;
		float feedback_;
		float modFreq_;
		float phase_;
		int widthSamples_;
		int delaySamples_;
		int MAXWIDTHSAMPLES_;
		float fs_;
};

inline float FlangerDAFX::process(float in) {
	float out = 0.0f;
	
	writePointer_++;
	
	if (writePointer_ >= delayLine_.size()) {
		writePointer_ = 0;
	}
	
	// Write the current input and feedback of a delayed sample to the delay line.
	int fbIndex = (writePointer_ - delaySamples_ + delayLine_.size()) % delayLine_.size();
	delayLine_[writePointer_] = in + feedback_ * delayLine_[fbIndex];
	
	// calculate the phase increament of the modulation sinusoid for each frame
	phase_ += 2*M_PI * modFreq_ / fs_;
	
	// wrap phase
	while (phase_ >= 2*M_PI) {
		phase_ -= 2*M_PI;
	}
	
	float mod = sinf_neon(phase_);
	
	// calculate current index m and frac to be used in interpolation
	readPointer_ = writePointer_ - (delaySamples_ + widthSamples_ * mod);
	
	while (readPointer_ < 0.0f) {
		readPointer_ += delayLine_.size();
	}
	
	int m = floor(readPointer_);
	float frac = readPointer_ - m;
	
	// linear interpolation from feedward delay
	out = delayLine_[(m-1 + delayLine_.size()) % delayLine_.size()]*frac + delayLine_[m]*(1-frac);
	
	// calculate output as sum of delayed and dry signal
	out = feedforward_ * out + blend_ * delayLine_[writePointer_];
	
	// mix between flanger signal and unaffected input signal
	out = in * (1 - mix_) + out * (mix_);
	
	return out;
}