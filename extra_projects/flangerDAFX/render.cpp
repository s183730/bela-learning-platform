/**
 *	Flanger DAFX
 *	------------
 *	A project implementing the "FlangerDAFX" object. A stereo pair of flanger objects	
 *	are created and used to process the input frames.
 * 
 *	See "FlangerDAFX.h" for more information
 * 
 *	Author: Nikolai Linden-Vørnle
 */
#include <Bela.h>
#include <vector>
#include "FlangerDAFX.h"

std::vector<FlangerDAFX> flanger;

bool setup(BelaContext *context, void *userData)
{
	// checks if the Bela is in stereo I/O mode
	if (context->audioInChannels != 2 && context->audioOutChannels != 2) {
		return false;
	}
	
	// make a stereo pair of flanger effects
	flanger.resize(context->audioInChannels);
	
	FlangerDAFX::Settings settings {
		.width = 2.0e-3,	// modulation width in seconds
		.modFreq = 0.1,		// modulation frequency in Hertz
		.mix = 1.0,			// dry/wet mix (mix between unaffected and affected signal)
		.feedback = 0.7,	// feedback coefficient
		.fs = context->audioSampleRate,
	};
	
	for (unsigned int ch = 0; ch < flanger.size(); ch++) {
		flanger[ch].setup(settings);
	}
	
	return true;
}

void render(BelaContext *context, void *userData)
{
	for (unsigned int n = 0; n < context->audioFrames; n++) {
		for (unsigned int ch = 0; ch < context->audioInChannels; ch++) {
			audioWrite(context, n, ch, flanger[ch].process(audioRead(context, n, ch)));
		}
	}
}

void cleanup(BelaContext *context, void *userData)
{
}