/*
	Simple 'moving average' FIR-filter
	--------------------------------
	
	This program generates a moving average with the order given by the
	constant "gOrder". The input is summed to mono.
	
	(c) Nikolai Linden-Vørnle
*/
#include <Bela.h>

const int gOrder = 20;	// order of the moving average filter
float delayBuffer[gOrder] = {}; // initialize a buffer to contain the delayed input samples
int gBuffPointer = 0;

bool setup(BelaContext *context, void *userData)
{
	return true;
}

void render(BelaContext *context, void *userData)
{
	for (int n = 0; n < context->audioFrames; n++) {
		
		float x = 0;	// input variable
		float y = 0;	// output variable
		
		// read current input frame and convert to mono
		for (int ch = 0; ch < context->audioInChannels; ch++) {
			x += audioRead(context, n, ch);
		}
		// normalize the input with number of channels
		y = x / context->audioInChannels;
		
		// multiply with the order+1 average 
		y = y / (gOrder + 1);
		
		for (int z = 0; z < gOrder; z++) {
			// calculate the index to read from the delay buffer
			int index = gBuffPointer - z;
			
			// wrap the buffer pointer if index is out of bounds
			if (index < 0) {
				index += gOrder;
			}
			
			// calculate the output
			y += delayBuffer[index] / (gOrder + 1);
		}
		
		// store current input 'x' in the delay buffer.
		delayBuffer[gBuffPointer] = x;
		
		// increment the buffer pointer
		gBuffPointer++;
		
		// wrap the buffer pointer if it goes out of bounds
		if (gBuffPointer >= gOrder-1) {
			gBuffPointer = 0;
		}
		
		// write to output buffer
		for (int ch = 0; ch < context->audioOutChannels; ch++) {
			audioWrite(context, n, ch, y);
		}
		
	}
}

void cleanup(BelaContext *context, void *userData)
{
}