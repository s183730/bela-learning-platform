/***** VibratoDAFX.cpp *****/
#include "VibratoDAFX.h"

VibratoDAFX::VibratoDAFX() {
}

VibratoDAFX::~VibratoDAFX() {
}

void VibratoDAFX::setup(Settings& settings) {
	fs_ = settings.fs;
	
	// Max width of 5ms
	MAXWIDTHSAMPLES_ = round(0.005f*settings.fs);
	
	setModFreq(settings.modFreq);
	setWidth(settings.width);
	
	// initialize phase for modulation sinusoid
	phase_ = 0.0f;
	
	// initialize delayline and pointers
	writePointer_ = 0;
	readPointer_ = 0.0f;
	delayLine_.resize(2 + 3*MAXWIDTHSAMPLES_, 0.0f); // ensures enough space (S. Disch 1999)
}

void VibratoDAFX::setWidth(float width) {
	widthSamples_ = width * fs_;
	
	if (widthSamples_ > MAXWIDTHSAMPLES_) {
		widthSamples_ = MAXWIDTHSAMPLES_;
	} else if (widthSamples_ < 0) {
		widthSamples_ = 0;
	}
	
	delaySamples_ = widthSamples_;
}

void VibratoDAFX::setModFreq(float modFreq) {
	modFreq_ = modFreq;
}

float VibratoDAFX::getWidth() {
	return widthSamples_ / fs_;
}

float VibratoDAFX::getModFreq() {
	return modFreq_;
}