/***** VibratoDAFX.h *****/
#pragma once
#include <vector>
#include <cmath>
#include <libraries/math_neon/math_neon.h>

/**
 * \brief Vibrato DAFX
 * 
 *	Implimentation of a vibrato DAFX, with inspiration from S. Disch (Zölzer 2011).
 *	Author: Nikolai Linden-Vørnle
 */
class VibratoDAFX {
	public:
		struct Settings {
			float width;	///< Width of delay modulation (in seconds)
			float modFreq;	///< Frequency of modulation (in Hertz)
			float fs;		///< Sampling frequency (in Hertz)
		};
	
		/**
		 *	Constructor
		 */
		VibratoDAFX();
		
		/**
		 *	Deconstructor
		 */
		~VibratoDAFX();
		
		/**
		 *	Process one input sample through the VibratoDAFX object and return output.
		 */
		float process(float in);
		
		/**
		 *	Setup of the VibratoDAFX object. The struct #Settings is used initialize the vibrato.
		 */
		void setup(Settings& settings);
		
		/**
		 *	Sets the modulation width of the vibrato. If the \a width is bigger than the initialized width,
		 *	the width will be set the maximum width possible.
		 *	Typical values ranges between 0-3ms (S. Disch 1999)
		 */
		void setWidth(float width);
		
		/**
		 *	Sets the modulation frequency of the vibrato. Modulation frequencies between 0.1 and 5 Hz 
		 *	are typical good values (S. Disch 1999). 
		 */
		void setModFreq(float modFreq);
		
		/**
		 *	Returns the current modulation width of the vibrato.
		 */
		float getWidth();
		
		/**
		 *	Returns the current modulation frequency of the vibrato.
		 */
		float getModFreq();
	
	protected:
		std::vector<float> delayLine_;
		int writePointer_;
		float readPointer_;
		
		float modFreq_;
		float phase_;
		int widthSamples_;
		int delaySamples_;
		int MAXWIDTHSAMPLES_;
		float fs_;
};

inline float VibratoDAFX::process(float in) {
	float out = 0.0f;
	
	writePointer_++;
	
	if (writePointer_ >= delayLine_.size()) {
		writePointer_ = 0;
	}
	
	delayLine_[writePointer_] = in;
	
	// calculate the phase increament of the modulation sinusoid for each frame
	phase_ += 2*M_PI * modFreq_ / fs_;
	
	// wrap phase
	while (phase_ >= 2*M_PI) {
		phase_ -= 2*M_PI;
	}
	
	float mod = sinf_neon(phase_);
	
	// calculate current index m and frac to be used in interpolation
	readPointer_ = writePointer_ - (delaySamples_ + widthSamples_ * mod);
	
	while (readPointer_ < 0.0f) {
		readPointer_ += delayLine_.size();
	}
	
	int m = floor(readPointer_);
	float frac = readPointer_ - m;
	
	// linear interpolation
	out = delayLine_[(m-1 + delayLine_.size()) % delayLine_.size()]*frac + delayLine_[m]*(1-frac); 
	
	return out;
}