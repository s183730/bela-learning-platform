/*
	Vibrato DAFX
	------------
	Example project to show the use of the "VibratoDAFX" object.
	The vibrato object is initialized with a modulation width, in milliseconds,
	and a modulation frequency, in Hertz.
	
	Two vibrato objects are created to process the incoming audio in stereo.
	
	See "VibratoDAFX.h" for more information.
	
	Author: Nikolai Linden-Vørnle
*/

#include <Bela.h>
#include "VibratoDAFX.h"
#include <vector>

std::vector<VibratoDAFX> vibrato;

bool setup(BelaContext *context, void *userData)
{
	// makes sure that the Bela is set up for stereo processing
	if (context->audioInChannels != 2 && context->audioOutChannels != 2) {
		return false;
	}
	
	// initialize a pair of vibrato obejct for stereo processing.
	vibrato.resize(2);
	
	VibratoDAFX::Settings settings {
		.width = 5.0e-3,	// modulation width in seconds
		.modFreq = 1.5,		// modulation frequency in Hz
		.fs = context->audioSampleRate,
	};
	
	for (unsigned int ch = 0; ch < vibrato.size(); ch++) {
		vibrato[ch].setup(settings);
	}
	
	return true;
}

void render(BelaContext *context, void *userData)
{
	for (unsigned int n = 0; n < context->audioFrames; n++) {
		for (unsigned int ch = 0; ch < context->audioInChannels; ch++) {
			audioWrite(context, n, ch, vibrato[ch].process(audioRead(context, n, ch)));
		}
	}
}

void cleanup(BelaContext *context, void *userData)
{
}