/***** downsampling.h *****/
#pragma once
#include <Bela.h>

/**
 *	\brief An object used to downsample the input signal.
 * 
 *	This object can be used to audiate the effect of aliasing, by reducing the samplerate
 *	of the audio samples. This is done by only letting each n'th sample pass through, setting
 *	the rest of the samples to zero, decrease the sampling rate of the signal.
 * 
 *	Author: Nikolai Linden-Vørnle 2021
 */
class Downsampling {
	public:
	/**
	 *	Default constructor.
	 */
	Downsampling();
	
	/**
	 *	Constructor that takes @p samplingFactor as argument and initializes
	 *	the Downsampling object with this values.
	 */
	Downsampling(int samplingFactor);
	
	/**
	 *	Deconstructor.
	 */
	~Downsampling();
	
	/**
	 *	Setup function used to initialize the #Downsampling object.
	 */
	void setup(int samplingFactor);
	
	/**
	 *	Process on frame/sample through the #Downsampling object and returns the output.
	 */
	float process(float in);
	
	/**
	 *	Used to turn on/off the Downsampling object. if true, the object will just pass through
	 *	the input signal.
	 */
	void setBypass(bool bypass);
	
	/**
	 *	Returns the true if the Downsampling object is bypassed, otherwise false.
	 */
	bool getBypass();
	
	/**
	 *	Updates the downsampling factor of the Downsampling object.
	 */
	void setSamplingFactor(int dsFacter);
	
	/**
	 *	Returns the current downsampling factor in the #Downsampling object
	 */
	int getSamplingFactor();
	
	protected:
	int samplingFactor_;
	int state_;
	bool bypass_;
};

inline float Downsampling::process(float in) {
	if (state_ >= samplingFactor_) {
		state_ = 0;
	}
	if (bypass_) {
		return in;
	}
	if (state_ == 0) {
		state_++;
		return in;
	} else {
		state_++;
		return 0.0;
	}
}