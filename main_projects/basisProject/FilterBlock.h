/***** filterBlock.h *****/
#pragma once
#include <libraries/Biquad/Biquad.h>
#include <vector>

/**
 *	\brief Biquad filter with 2nd, 4th, 6th and 8th order.
 * 
 *	This class is used to generate different types of filters, using the
 *	'Biquad' library. The filter types and order can be select and modified
 *	in real time.
 *	
 *	Author: Nikolai Linden-Vørnle 2021
 */
class FilterBlock {
	public:
		typedef enum {
			second = 0,
			fourth, 
			sixth,
			eighth
		} FilterOrder;
		typedef enum {
			lowpass = 0,
			highpass,
			bandpass,
			bandstop
		} FilterState;
		
		/**
		 *	Constructor
		 */
		FilterBlock();
		
		/**
		 *	Deconstructor
		 */
		~FilterBlock();
		
		/**
		 *	Setup function. initialize biquad objects and members.
		 */
		int setup(double fs, double cutoff, double q);
		
		/**
		 *	Process sample an return output. Update member states.
		 */ 
		float process(float in);
		
		/**
		 *	Get the current state/filter type of the filterBlock. 
		 */
		FilterState getState();
		
		/**
		 *	Get the current order of the filter.
		 */
		FilterOrder getOrder();
		
		/**
		 *	Cycle through filter states. The cycle is:
		 *	lowpass -> highpass -> bandpass -> bandstop -> lowpass ...
		 */
		void switchState();
		
		/**
		 *	Cycle through filter order. The cycle is:
		 *	2nd order -> 4th order -> 6th order -> 8th order -> 2nd order ...
		 */
		void switchOrder();
		
		/**
		 *	Set the current filter state/type.
		 */
		void setState(FilterBlock::FilterState state);
		
		/**
		 *	Set the current filter order.
		 */ 
		void setOrder(FilterBlock::FilterOrder order);
		
		/**
		 *	Reset Biquad objects.
		 */
		void resetBiquad();
		
		/**
		 *	Set the Q-factor for the Biquad objects.
		 */
		void setQ(double q);
		
		/**
		 *	Set the cutoff frequency of the Biquad objects.
		 */
		void setFc(double fc);
	
	protected:
		FilterState state_;
		FilterOrder order_;
		std::vector<Biquad> filters_;
};

inline float FilterBlock::process(float in) {
	float out = 0.0;
	
	out = filters_[0].process(in);
	
	for (int i = 1; i <= order_; i++) {
		out = filters_[i].process(out);
	}
	
	return out;
}