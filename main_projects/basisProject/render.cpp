/*
	Biquad filter of different orders & downsampling
	--------------------------------------------------------
	This project gives the user the ability to switch between lowpass, highpass, bandpass
	and bandstop filters of 2nd, 4th, 6th and 8th order, aswell as downsampling the input signal. 
	
	FILTER
	--------------------------------------------------------
	Every time the user presses one of the "state" or "order" buttons (D0 and D1), the state 
	of the filter or order of the filter is changed, cycling through the different modes. 
	The system starts as a 2nd order lowpass filter.
	
	LED's are used to indicate the state and order of the filter. The settings are:
	
	STATE LED (A_out0):
	not lit = lowpass
	1/3 lit = highpass
	2/3 lit = bandpass
	fully lit = bandstop
	
	ORDER LED (A_out1):
	not lit = 2nd order
	1/3 lit = 4th order
	2/3 lit = 6th order
	fully lit = 8th order
	
	Potentiometers (A_in0 and A_in1) are used to control the cutoff frequency and Q factor of the filter.
	The constants "kFcMIN" and "kFcMAX" are used to set the minimum and maximum cutoff frequency for 
	the filter respectivly. "kQMAX" is used to set the maximum Q factor of the filters. Beware of high Q 
	values, since this will cause oscillations/instability.
	
	DOWNSAMPLING
	--------------------------------------------------------
	The downsampling objects can be switch on and off by pressing button D2. When the objects is on,
	the input signal will be downsampled by a factor given by the position of potentiometer A_in2.
	The range for the potentiometer is set by the constant "kDSMAX", by setting the maximum downsampling
	factor.
	
	The 'Biquad' library is used to create the Biquad structures, implementing
	the filters. The audio is processed in stereo.
	
	Author: Nikolai Linden-Vørnle, 2021
*/
#include <Bela.h>
#include "FilterBlock.h"
#include "Downsampling.h"
#include <libraries/Debounce/BelaDebounce.h>
#include <vector>
#include <cmath>

// Stereo pair of filter blocks
std::vector<FilterBlock> filters(2);

// Stereo pair of downsampling objects
std::vector<Downsampling> downsampling(2);

// Constant - filter and downsampling parameters
const float kFcMIN = 50.0;		// minimum cutoff frequency from potentiometer A_in0
const float kFcMAX = 12000.0;	// maximum cutoff frequency from potentiometer A_in0
const float kQMAX = 3.0;		// maximum Q factor of the filter object
const int kDSMAX = 4;			// maximum downsampling factor

// Constants - I/O, buttons, potentiometers, LEDs
const unsigned int kButtonState = 0;	// digital pin for state button
const unsigned int kButtenOrder = 1;	// digital pin for order button
const unsigned int kButtonDs = 2;		// digital pin for down sampling on/off
const unsigned int kLEDstate = 0;		// analog output pin for order LED
const unsigned int kLEDorder = 1;		// analog output pin for state LED
const unsigned int kLEDds = 2;			// analog output pin for down sample LED
const unsigned int kPotFreq = 0;		// analog input pin for frequency potentiometer
const unsigned int kPotQ = 1;			// analog input pin for Q factor potentiometer
const unsigned int kPotDS = 2;			// analog input pin for downsampling factor

// Global variable used to for edge detection on buttons
int gLastReadingStateButton = 1;
int gLastReadingOrderButton = 1;
int gLastReadingDSButton = 1;

// Debounce objects for digital inputs
BelaDebounce gDebounceStateButton;
BelaDebounce gDebounceOrderButton;
BelaDebounce gDebounceDSButton;
const double kDebounceMs = 100.0;

// Variable used for linear to exponential conversion (cutoff frequency)
float gFcScaling;

// used to auto-tune reading from analogRead() for precise cutoff frequency calculation
float gMaxRead;

// counter for printing of cutoff frequency from pot A_in0
unsigned int gPrintCount;

bool setup(BelaContext *context, void *userData)
{
	// Checks if there is enough analog in/out pins enabled
	if (context->analogInChannels < 4 || context->analogOutChannels < 4) {
		return false;
	}
	
	// initialize filter block objects
	for (unsigned int ch = 0; ch < filters.size(); ch++) {
		filters[ch].setup(context->audioSampleRate, 2000.0, 0.707);
	}
	
	// initialize downsampling objects 
	for (unsigned int ch = 0; ch < downsampling.size(); ch++) {
		downsampling[ch].setup(1);
	}
	
	// set print counter to 0
	gPrintCount = 0;
	
	// initialize the auto-tune variable to the default 3.3f/4.096f
	gMaxRead = 3.3f/4.096f;
	
	// initialize digital pins as debounced inputs
	BelaDebounce::Settings settings {
		.context = context,
		.channel = kButtonState,
		.debounceMs = kDebounceMs
	};
	
	gDebounceStateButton.setup(settings);
	settings.channel = kButtenOrder;
	gDebounceOrderButton.setup(settings);
	settings.channel = kButtonDs;
	gDebounceDSButton.setup(settings);
	
	gFcScaling = log2(kFcMAX / kFcMIN);
	
	return true;
}

void render(BelaContext *context, void *userData)
{
	//*** BUTTONS ***//
	int currentReadingStateButton = (int)gDebounceStateButton.process(context);
	int currentReadingOrderButton = (int)gDebounceOrderButton.process(context);
	int currentReadingDSButton = (int)gDebounceDSButton.process(context);
	
	// checks if the 'state' button has been pressed and change filter state (D0)
	if (currentReadingStateButton == 0 && gLastReadingStateButton == 1) {
		for (unsigned int ch = 0; ch < filters.size(); ch++) {
			filters[ch].switchState();
		}
	}
	
	// checks if the 'order' button has been pressed and change the order (D1)
	if (currentReadingOrderButton == 0 && gLastReadingOrderButton == 1) {
		for (unsigned int ch = 0; ch < filters.size(); ch++) {
			filters[ch].switchOrder();
		}
	}
	
	// checks if the 'downsampling' button has been pressed and toggles bypass (D2)
	if (currentReadingDSButton == 0 && gLastReadingDSButton == 1) {
		for (unsigned int ch = 0; ch < downsampling.size(); ch++) {
			if (downsampling[ch].getBypass()) {
				downsampling[ch].setBypass(false);
			} else {
				downsampling[ch].setBypass(true);
			}
		}	
	}
	
	// store current state for comparison in next block
	gLastReadingStateButton = currentReadingStateButton;
	gLastReadingOrderButton = currentReadingOrderButton;
	gLastReadingDSButton = currentReadingDSButton;
	
	//*** LEDs ***//
	// set the light intensity on LED0 to show the filter state (A_out0)
	FilterBlock::FilterState state = filters[0].getState();
	if (state == FilterBlock::lowpass) {
		analogWrite(context, 0, kLEDstate, 0.2); 
	} else if (state == FilterBlock::highpass) {
		analogWrite(context, 0, kLEDstate, 0.5);
	} else if (state == FilterBlock::bandpass) {
		analogWrite(context, 0, kLEDstate, 0.65);
	} else if (state == FilterBlock::bandstop) {
		analogWrite(context, 0, kLEDstate, 1.0);
	} else {
		analogWrite(context, 0, kLEDstate, 0.0);
	}
	
	// set the light intensity on LED1 to show the filter order (A_out1)
	FilterBlock::FilterOrder order = filters[0].getOrder();
	if (order == FilterBlock::second) {
		analogWrite(context, 0, kLEDorder, 0.2);
	} else if (order == FilterBlock::fourth) {
		analogWrite(context, 0, kLEDorder, 0.5);
	} else if (order == FilterBlock::sixth) {
		analogWrite(context, 0, kLEDorder, 0.65);
	} else if (order == FilterBlock::eighth) {
		analogWrite(context, 0, kLEDorder, 1.0);
	} else {
		analogWrite(context, 0, kLEDorder, 0.0);
	}
	
	// toggles LED2 on/off depending on downsampling being bypassed or not (A_out2)
	if (downsampling[0].getBypass()) {
		analogWrite(context, 0, kLEDds, 0.0);
	} else {
		analogWrite(context, 0, kLEDds, 1.0);
	}
	
	//*** POTENTIOMETERS ***//
	// set the cutoff frequency in the filter objects (A_i,n0)
	float analogRead0 = analogRead(context, 0, kPotFreq);
	gMaxRead = fmax(gMaxRead, analogRead0);	// auto-tune maximum value
	float freq = map(analogRead0, 0.0, gMaxRead, 0.0, gFcScaling);
	freq = kFcMIN*powf(2, freq);
	for (unsigned int i = 0; i < filters.size(); i++) {
		filters[i].setFc(freq);
	}
	
	// print f_c every other second
	if (gPrintCount >= (int)context->audioSampleRate*2) {
		rt_printf("Calculated f_c from analogRead(): %f\n", freq);
		gPrintCount = 0;
	}
	gPrintCount += context->audioFrames;
	
	// set the Q factor for the filter object (A_in1)
	float Qfactor = map(analogRead(context, 0, kPotQ), 0.0f, 3.3f/4.096f, 0.707, kQMAX);
	for (unsigned int i = 0; i < filters.size(); i++) {
		filters[i].setQ(Qfactor);
	}
	
	// set the downsampling factor for the downsampling object (A_in2)
	int dsFactor = map(analogRead(context, 0, kPotDS), 0.0f, 3.3f/4.096f, 1, kDSMAX);
	for (unsigned int ch = 0; ch < downsampling.size(); ch++) {
		downsampling[ch].setSamplingFactor(dsFactor);
	}
	
	// PROCESS
	for (unsigned int n = 0; n < context->audioFrames; n++) {
		float in = 0, out = 0;
		for (unsigned int ch = 0; ch < context->audioInChannels; ch++) {
			in = audioRead(context, n, ch);
			out = filters[ch].process(in);			// process audio frame/sample through filter object
			out = downsampling[ch].process(out);	// process audio frame/sample through downsampling object
			audioWrite(context, n, ch, out);
		}
	}
}

void cleanup(BelaContext *context, void *userData)
{
}