#pragma once
#include <vector>

/**
 *	\brief Delay - Digital Audio Effect (DAFX)
 * 
 *	A delay DAFX effect with blend, delay time and feedback control.
 *	Author: Nikolai Linden-Vørnle 2021
 */ 
class DelayDAFX {
	public:
		struct Settings {
			float maxDelayTime;	///< Maximum delay time for the object. (in seconds)
			float delayTime;	///< Delay time for the object. (in seconds)
			float mix;			///< Mix value between dry and wet signal. (range from 0.0 to 1.0)
			float feedback;		///< Feedback amount from output. (range from 0.0 to <1.0.)
			float fs;			///< Sampling frequency of the project.
		};
		
		/**
		 *	Constructor for the DelayDAFX object. 
		 */
		DelayDAFX();
		
		/**
		 *	Deconstructor for the DelayDAFX object.
		 */ 
		~DelayDAFX();
		
		/**
		 *	Pass a Settings struct to the setup function to initialize the
		 *	DelayDAFX object.
		 */ 
		void setup(const Settings &settings);
		
		/**
		 *	Process one sample through the DelayDAFX object.
		 */ 
		float process(float in);
		
		/**
		 *	Sets the delay time of the filter in seconds
		 *	@param delayTime The delay time should be no longer than the maxDelayTime_
		 */
		void setDelayTime(float delayTime);
		
		/**
		 *	Sets the blend between the dry (direct signal) and wet (delayed signal).
		 *	Valid values of @p mix ranges between 0.0 and 1.0, with 0.0 giving
		 *	100% dry signal and 1.0 giving a 100% wet signal.
		 */ 
		void setMix(float mix);		
		
		/**
		 *	Sets the amount of feedback in the DelayDAFX object.
		 */ 
		void setFeedback(float feedback);
		
		/**
		 *	Returns the current delay time of the object.
		 */
		float getDelayTime();
		
		/**
		 *	Returns the current mix value for the object.
		 */
		float getMix();
		
		/**
		 *	Returns the current feeback coefficient for the object.
		 */
		float getFeedback();
		
		/**
		 *	initialize delay line and read/write pointers.
		 */
		void resetDelay();
		
	protected:
		int maxDelaySamples_;
		int delaySamples_;
		float mix_;
		float feedback_;
		float fs_;
		int readPointer_;
		int writePointer_;
		std::vector<float> delayBuffer_;
};

inline float DelayDAFX::process(float in) {
	float out = 0.0f;
	
	writePointer_++;
	
	if (writePointer_ >= maxDelaySamples_) {
		writePointer_ = 0;
	}
	
	readPointer_ = (writePointer_ - delaySamples_ + maxDelaySamples_) % maxDelaySamples_;
	
	delayBuffer_[writePointer_] = in + feedback_ * delayBuffer_[readPointer_];
	
	out = (1 - mix_) * in + mix_ * delayBuffer_[readPointer_];
	
	return out;
}