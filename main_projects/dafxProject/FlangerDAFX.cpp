/***** FlangerDAFX.cpp *****/
#include "FlangerDAFX.h"

FlangerDAFX::FlangerDAFX() {
}
 
FlangerDAFX::~FlangerDAFX() {
}

void FlangerDAFX::setup(Settings &settings) {
	fs_ = settings.fs;
	
	// Max width of 2ms (+/- 1.5ms)
	MAXWIDTHSAMPLES_ = round(0.002f*fs_);
	
	setWidth(settings.width);
	setModFreq(settings.modFreq);
	setMix(settings.mix);
	setFeedback(settings.feedback);
	
	// The blend and feedforward coefficients are selected as typical industry standard values (S. Disch 1999)
	blend_ = 0.7f;
	feedforward_ = 0.7f;
	
	// initialize phase for modulation sinusoid
	phase_ = 0.0;
	
	// initialize delayline and pointers
	writePointer_ = 0;
	readPointer_ = 0.0f;
	delayLine_.resize(2 + 3*MAXWIDTHSAMPLES_, 0.0f);
}

void FlangerDAFX::setWidth(float width) {
	widthSamples_ = width * fs_;
	
	if (widthSamples_ > MAXWIDTHSAMPLES_) {
		widthSamples_ = MAXWIDTHSAMPLES_;
	}
	
	delaySamples_ = widthSamples_;
}

void FlangerDAFX::setModFreq(float modFreq) {
	modFreq_ = modFreq;
}

void FlangerDAFX::setMix(float mix) {
	if (mix > 1.0f) {
		mix = 1.0f;
	} else if (mix < 0.0f) {
		mix = 0.0f;
	} else {
		mix_ = mix;
	}
}

void FlangerDAFX::setFeedback(float feedback) {
	if (feedback >= 1.0f) {
		feedback_ = 0.99f;
	} else if (feedback < 0.0f) {
		feedback_ = 0.0f;
	} else {
		feedback_ = feedback;
	}
}
	
float FlangerDAFX::getWidth() {
	return widthSamples_ / fs_;	
}
	
float FlangerDAFX::getModFreq() {
	return modFreq_;
}

float FlangerDAFX::getMix() {
	return mix_;
}

float FlangerDAFX::getFeedback() {
	return feedback_;
}