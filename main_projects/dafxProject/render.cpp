/*
	Digital Audio Effects (DAFX) - real-time
 	----------------------------------------
 	This project contains 3 different DAFX classes, which can be used and
 	selected in real-time on the Bela system.
 	
 	If one of the three buttons is pressed, the corrosponding effects parameters can be 
 	edited (see table below). This allows for real-time control of the DAFX with 
 	limited amount of hardware. 
 	
 	HARDWARE LAYOUT:
 	----------------
 	Below is a table of the control interface. Depending on which button is pressed
 	the Potentiometer will behave differently. The LED on the button will be lit when the
 	button is pressed.
 	
 	| Effect Type/Input Potentiometer | Analog IN 0 | Analog IN 1 | Analog IN 2          |
	|---------------------------------|-------------|-------------|----------------------|
	| Delay      D2                   | mix         | delay time  | feedback             |
	| Flanger    D1                   | mix         | width       | modulation frequency |
	| Vibrato    D0                   | ---         | width       | modulation frequency |
	
	The 'OnePole' library is used to make 1st order filtering of the analog values read 
	from the potentiometers. 
  
	Author: Nikolai Linden-Vørnle 2021
 */

#include <Bela.h>
#include "DelayDAFX.h"
#include "FlangerDAFX.h"
#include "VibratoDAFX.h"
#include <vector>
#include <libraries/OnePole/OnePole.h>

// OnePole filter - analog voltage filtering
std::vector<OnePole> inputFilters;

// Constants - Potentiometers, push-buttons, LEDs
const unsigned int kPot0 = 0;
const unsigned int kPot1 = 1;
const unsigned int kPot2 = 2;
const unsigned int kPushButton0 = 0;
const unsigned int kPushButton1 = 1;
const unsigned int kPushButton2 = 2;
const unsigned int kLED0 = 0;
const unsigned int kLED1 = 1;
const unsigned int kLED2 = 2;

// DAFX objects
std::vector<DelayDAFX> delay;
std::vector<FlangerDAFX> flanger;
std::vector<VibratoDAFX> vibrato;

// Constants for DAFX objects
const float kMaxDelayTime = 0.5;		// Max delay time (in seconds)
const float kMaxWidthFlanger = 2.0e-3;	// Max modulation width for the flanger effect (in seconds)
const float kMaxWidthVibrato = 3.0e-3;	// Max modulation width for the vibrato effect (in seconds)
const float kMaxModFreq = 4;			// Max modulation frequency of flanger and vibrato effect (in Hertz)

bool setup(BelaContext *context, void *userData)
{
	// Initialize smoothing filters for potentiometers
	inputFilters.resize(3);
	for (unsigned int i = 0; i < inputFilters.size(); i++) {
		inputFilters[i].setup(30.0f, context->audioSampleRate, OnePole::LOWPASS);
	}
	
	// Checks if there is enough analog in/out pins enabled
	if (context->analogInChannels < 4 || context->analogOutChannels < 4) {
		return false;
	}
	
	// Initialize delay objects
	DelayDAFX::Settings settingsDelay {
		.maxDelayTime = kMaxDelayTime,	// maximum delay time for the delay object
		.delayTime = 0.2f,				// delay time
		.mix = 0.0f,					// dry/wet mix (0 = unprocessed signal, 1 = only delayed signal)
		.feedback = 0.4f,				// feedback amount
		.fs = context->audioSampleRate,
	};
	delay.resize(2);
	for (int i = 0; i < delay.size(); i++) {
		delay[i].setup(settingsDelay);
	}
	
	// Initialize flanger objects
	FlangerDAFX::Settings settingsFlanger {
		.width = 1.5e-3,	// modulation width of 1.5ms
		.modFreq = 0.5f,	// modulation frequency
		.mix = 0.0f,		// dry/wet mix (0 = unprocessed signal, 1 = flanged signal)
		.feedback = 0.6f,	// feedback coefficient from delayline
		.fs = context->audioSampleRate,
	};
	flanger.resize(2);
	for (int i = 0; i < flanger.size(); i++) {
		flanger[i].setup(settingsFlanger);
	}
	
	// Initialize vibrato objects
	VibratoDAFX::Settings settingsVibrato {
		.width = 0.0f,		// modulation width of 0ms
		.modFreq = 1.0f,	// modulation frequency
		.fs = context->audioSampleRate,
	};
	vibrato.resize(2);
	for (int i = 0; i < vibrato.size(); i++) {
		vibrato[i].setup(settingsVibrato);
	}
	
	// Set digital pins to INPUT
	pinMode(context, 0, kPushButton0, INPUT);
	pinMode(context, 0, kPushButton1, INPUT);
	pinMode(context, 0, kPushButton2, INPUT);
	
	return true;
}

void render(BelaContext *context, void *userData)
{
	// read lowpassed analog values from potentiometers
	float pot0Value = inputFilters[kPot0].process(analogRead(context, 0, kPot0));
	float pot1Value = inputFilters[kPot1].process(analogRead(context, 0, kPot1));
	float pot2Value = inputFilters[kPot2].process(analogRead(context, 0, kPot2));
	
	// Button D0 is pressed
	if (digitalRead(context, 0, kPushButton0) == 0) {
		// Turn LED on when button is pressed
		analogWrite(context, 0, kLED0, 1);
		
		// Update vibrato parameters
		float vibratoWidth = map(pot1Value, 0.0f, 3.3f/4.096f, 0.0f, kMaxWidthVibrato);
		if (vibrato[0].getWidth() != vibratoWidth) {
			for (unsigned int ch = 0; ch < vibrato.size(); ch++) {
				vibrato[ch].setWidth(vibratoWidth);
			}
		}
		
		float vibratoModFreq = map(analogRead(context, 0, kPot2), 0.0f, 3.3f/4.096f, 0.0f, kMaxModFreq);
		if (vibrato[0].getModFreq() != vibratoModFreq) {
			for (unsigned int ch = 0; ch < vibrato.size(); ch++) {
				vibrato[ch].setModFreq(vibratoModFreq);
			}
		}
	} else {
		// Turn LED off when button is released
		analogWrite(context, 0, kLED0, 0);
	}
	
	// Button D1 is pressed
	if (digitalRead(context, 0, kPushButton1) == 0) {
		// Turn on LED when button is pressed
		analogWrite(context, 0, kLED1, 1);
		
		// Update flanger parameters
		float flangerMix = map(pot0Value, 0.0f, 3.3f/4.096f, 0.0f, 1.0f);
		if (flanger[0].getMix() != flangerMix) {
			for (unsigned int ch = 0; ch < flanger.size(); ch++) {
				flanger[ch].setMix(flangerMix);
			}
		}
		
		float flangerWidth = map(pot1Value, 0.0f, 3.3f/4.096f, 0.0f, kMaxWidthFlanger);
		if (flanger[0].getWidth() != flangerWidth) {
			for (unsigned int ch = 0; ch < flanger.size(); ch++) {
				flanger[ch].setWidth(flangerWidth);
			}
		}
		
		float flangerModFreq = map(pot2Value, 0.0f, 3.3f/4.096f, 0.0f, kMaxModFreq);
		if (flanger[0].getModFreq() != flangerModFreq) {
			for (unsigned int ch = 0; ch < flanger.size(); ch++) {
				flanger[ch].setModFreq(flangerModFreq);
			}
		}
	} else {
		// Turn LED off when button is released
		analogWrite(context, 0, kLED1, 0);
	}
	
	// Button D2 is pressed
	if (digitalRead(context, 0, kPushButton2) == 0) {
		// Turn LED on when button is pressed
		analogWrite(context, 0, kLED2, 1);
		
		// Update delay parameters
		float delayMix = map(pot0Value, 0.0f, 3.3f/4.096f, 0.0f, 1.0f);
		if (delay[0].getMix() != delayMix) {
			for (unsigned int ch = 0; ch < delay.size(); ch++) {
				delay[ch].setMix(delayMix);
			}
		}
		
		float delayTime = map(pot1Value, 0.0f, 3.3f/4.096f, 0.0f, kMaxDelayTime);
		if (delay[0].getDelayTime() != delayTime) {
			for (unsigned int ch = 0; ch < delay.size(); ch++) {
				delay[ch].setDelayTime(delayTime);
			}
		}
		
		float delayFeedback = map(pot2Value, 0.0f, 3.3f/4.096f, 0.0f, 0.95f);
		if (delay[0].getFeedback() != delayFeedback) {
			for (unsigned int ch = 0; ch < delay.size(); ch++) {
				delay[ch].setFeedback(delayFeedback);
			}
		}
	} else {
		// Turn LED off when button is released
		analogWrite(context, 0, kLED2, 0);
	}
	
	// Process audio frames
	for (unsigned int n = 0; n < context->audioFrames; n++) {
		for (unsigned int ch = 0; ch < context->audioInChannels; ch++) {
			float in = audioRead(context, n, ch);
			float out = 0.0f;
			out = vibrato[ch].process(in);		// process through vibrato effect
			out = flanger[ch].process(out);		// process through flanger effect
			out = delay[ch].process(out);		// process through delay/echo effect
			audioWrite(context, n, ch, out);
		}
	}
}

void cleanup(BelaContext *context, void *userData)
{
}