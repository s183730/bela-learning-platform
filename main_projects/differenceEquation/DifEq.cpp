/***** DifEq.cpp *****/
#include "DifEq.h"

DifEq::DifEq() {
}

DifEq::~DifEq() {
}

bool DifEq::setup(std::vector<double> &a, std::vector<double> &b) {
	
	a_.resize(1, 0.0);
	
	for (unsigned int i = 1; i < a.size(); i++) {
		if (a[0] == 1 && a[i] != 0.0) {
			a_.resize(a.size(), 0.0);
		}
	}
	
	b_.resize(b.size(), 0.0);
	
	for (unsigned int i = 0; i < a_.size(); i++) {
		a_[i] = a[i];
	} 
	
	for (unsigned int i = 0; i < b_.size(); i++) {
		b_[i] = b[i];
	}
	
	if (a_.size() > 1) {
		feedBack_.resize(a_.size() - 1, 0.0);
	} else {
		feedBack_.resize(1, 0.0);
	}
	fbPointer_ = 0;
	
	if (b_.size() > 1) {
		feedForward_.resize(b_.size() - 1, 0.0);		
	} else {
		feedForward_.resize(1, 0.0);
	}
	ffPointer_ = 0;
	
	return true;
}

void DifEq::setAcoef(unsigned int index, double coef) {
	a_[index] = coef;	
}

void DifEq::setBcoef(unsigned int index, double coef) {
	b_[index] = coef;
}

double DifEq::getAcoef(unsigned int index) {
	return a_[index];	
}

double DifEq::getBcoef(unsigned int index) {
	return b_[index];
}

unsigned int DifEq::getAsize() {
	return feedBack_.size();
}

unsigned int DifEq::getBsize() {
	return feedForward_.size();
}

