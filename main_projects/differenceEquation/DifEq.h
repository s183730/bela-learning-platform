/***** DifEq.h *****/
/*
	A class used to generate FIR/IIR filters from difference equation coefficients.
	The class uses circular buffers to implement the delay line.
	
	std::vector<double> arrays with the 'a' and 'b' coefficients are passed to the setup
	to initialize the object. the process() function is called for every frame in the
	render() in render.cpp
	
	Author: Nikolai Linden-Vørnle, 2021
*/
#pragma once
#include <vector>

/**
 *	\brief Direct implimentation of difference equations.
 * 
 *	A class used to impliment difference equations from 'a' and 'b' coefficients.
 *	NOTE: this is not a memory and computational efficient class, so high order 
 *	difference equations should be avoided.
 */
class DifEq {
	public:
		/**
		 *	Constructor
		 */
		DifEq();
		
		/**
		 *	Deconstructor
		 */
		~DifEq();
		
		/**
		 *	Process an incoming frame/sample and get the filtered sampled returned
		 */
		float process(float in);
		
		/**	
		 *	Initialize the DifEq object, setting the a and b coefficients
		 *	and allocate memory for the delayed input/outputs and reset pointers.
		 */
		bool setup(std::vector<double> &a, std::vector<double> &b);
		
		/**
		 *	Set 'a' coefficient in the object at a given index.
		 */
		void setAcoef(unsigned int index, double coef);
		
		/**
		 *	Set 'b' coefficient in the object at a given index
		 */
		void setBcoef(unsigned int index, double coef);
		
		/**
		 *	Get the 'a' coefficient at a giving index stored in the object.
		 */
		double getAcoef(unsigned int index);
		
		/**
		 *	Get the 'b' coefficient at a giving index stored in the object.
		 */
		double getBcoef(unsigned int index);
		
		/**
		 *	Get the size of feedback delay line implimented by the object.
		 */
		unsigned int getAsize();
		
		/**
		 *	Get the size of feedforward delay line implimented by the object.
		 */
		unsigned int getBsize();
	
	protected:
		std::vector<double> a_;
		std::vector<double> b_;
		std::vector<double> feedForward_;
		std::vector<double> feedBack_;
		unsigned int ffPointer_;
		unsigned int fbPointer_;
};

/**
 * 
 */
inline float DifEq::process(float in) {
	// reset y[n]
	float out = 0.0;
	
	// add current input b_0*x[n] to the output y[n]
	out += in*b_[0];
	
	// add all feedward components (delayed inputs x[n-i])
	for (unsigned int i = 1; i < b_.size(); i++) {
		int index = ffPointer_ - i + 1;
		
		if (index < 0) {
			index += feedForward_.size();
		}
		
		out += b_[i]*feedForward_[index];
	}

	// subtract all feedback components (delayed outputs y[n-i])
	for (unsigned int i = 1; i < a_.size(); i++) {
		int index = fbPointer_ - i + 1;
		
		if (index < 0) {
			index += feedBack_.size();
		}
		
		out -= a_[i]*feedBack_[index];
	}
	
	// increment buffer pointers and store current in/out values
	ffPointer_++;
	if (ffPointer_ == feedForward_.size()) {
		ffPointer_ = 0;
	}
	feedForward_[ffPointer_] = in;
	
	fbPointer_++;
	if (fbPointer_ == feedBack_.size()) {
		fbPointer_ = 0;
	}
	feedBack_[fbPointer_] = out;
	
	return out;
}