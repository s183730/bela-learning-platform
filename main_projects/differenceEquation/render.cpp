/*
	Difference Equations
	--------------------
	
	This project implimentes IIR/FIR filters using 'a' and 'b' coeffients from a
	difference equations. An array of of 'a' and 'b' coeffients are passed to the 
	objects 'DifEq', using the setup() function. The object can be used to 
	impliment IIR/FIR filters by usning the procss() function in the render() function.
	
	if 'useTXTfiles' is set to TRUE, the filter coeffients in the .txt files will be 
	used to initialize the difference equation filter. 
	
	The filter coeffients are stored in two seperate .txt files, "a.txt" and "b.txt".
	You can upload your own to the project to impliment different filters.
	
	If 'useTXTfiles' is set to FALSE, the user can fill the 'a' and 'b' arrays in 
	the code by themselfs.
	
	NOTICE:
	The .txt files needs to contain only the filter coeffients, seperated by a new
	line (take a look at the files). The coeffients are ordered from index 0, e.g a[0], 
	at the top of the file, to the highest coeffients at the bottom of the .txt file.
	
	--When dealing with IIR filters--
	The order should NOT exceed ~127 (coeffients arrays of length 128 = 2^{7}). sizes 
	above this will course the Bela to crash, do to too many computations.
	
	--When dealing with FIR filters--
	The order should not exceed -255 (coeffients arrays of length 256 = 2^{8}). sizes 
	above this will course Bela to crash, do to too many computations.
	
	The audio is processed in mono.
	
	Author: Nikolai Linden-Vørnle, 2021
*/

#include <Bela.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "DifEq.h"

DifEq filter;

std::vector<double> a, b;

const bool useTXTfiles = true;

bool setup(BelaContext *context, void *userData)
{
	// makes sure that the Bela runs in stereo I/O
	if (context->audioInChannels != 2 || context->audioOutChannels != 2) {
		return false;
	}
	
	if (useTXTfiles) {
		// get the 'a' filter coeffients from the "a.txt" file
		std::ifstream readFile;
		readFile.open("a.txt");
		if (readFile.is_open()) {
			std::string temp;
			while(getline(readFile, temp)) {
				a.push_back(std::stod(temp));
		}
		readFile.close();
		}
	
		// get the 'b' filter coeffients from the "b.txt" file
		readFile.open("b.txt");
		if (readFile.is_open()) {
			std::string temp;
			while(getline(readFile, temp)) {
				b.push_back(std::stod(temp));
			}
			readFile.close();
		}	
	} else {
		// initialize using arrays instead of .txt files
		a.resize(1);
		a[0] = 0;
		b.resize(1);
		b[0] = 1;
	}
	
	// initialize the DifEq object with the 'a' and 'b' arrays
	if (!filter.setup(a, b)) {
		return false;
	}
	
	rt_printf("size of a: %d\n", filter.getAsize());
	rt_printf("size of b: %d\n", filter.getBsize());
	
	rt_printf("setup complete!\n");
	
	return true;
}

void render(BelaContext *context, void *userData)
{
	for (unsigned int n = 0; n < context->audioFrames; n++) {
		float in = 0.0, out = 0.0;
		
		for (unsigned int ch = 0; ch < context->audioInChannels; ch++) {
			in += audioRead(context, n, ch);	
		}
		
		in = in / context->audioInChannels;
		
		out = filter.process(in);
		
		for (unsigned int ch = 0; ch < context->audioOutChannels; ch++) {
			audioWrite(context, n, ch, out);
		}
	}
}

void cleanup(BelaContext *context, void *userData)
{
}