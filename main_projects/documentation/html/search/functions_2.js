var searchData=
[
  ['getacoef_65',['getAcoef',['../class_dif_eq.html#a32e2f945e9a5146831f98043c4502c09',1,'DifEq']]],
  ['getasize_66',['getAsize',['../class_dif_eq.html#a707e927037d717f5f44900b64f366f3f',1,'DifEq']]],
  ['getbcoef_67',['getBcoef',['../class_dif_eq.html#a05f8d6cc20c1b3ae76ad0250d9e9342c',1,'DifEq']]],
  ['getbsize_68',['getBsize',['../class_dif_eq.html#a04ae0cfd4eeb9853e05d8feb7ecf9f09',1,'DifEq']]],
  ['getbypass_69',['getBypass',['../class_downsampling.html#a47283258b846b464aa6b2983424ac6cd',1,'Downsampling']]],
  ['getdelaytime_70',['getDelayTime',['../class_delay_d_a_f_x.html#a8ec047d10fffa5b19b99006644a4fb5e',1,'DelayDAFX']]],
  ['getfeedback_71',['getFeedback',['../class_delay_d_a_f_x.html#a9fb95194e045dff27ed6007492954e3d',1,'DelayDAFX::getFeedback()'],['../class_flanger_d_a_f_x.html#a57023bb07d6988a3c3d54b30bf11becf',1,'FlangerDAFX::getFeedback()']]],
  ['getmix_72',['getMix',['../class_delay_d_a_f_x.html#afda9af78ac821b0659bc091c0b008a60',1,'DelayDAFX::getMix()'],['../class_flanger_d_a_f_x.html#a7cfdfb42b7506df300d9f1922369a675',1,'FlangerDAFX::getMix()']]],
  ['getmodfreq_73',['getModFreq',['../class_flanger_d_a_f_x.html#a2c1030ac80dde1769eede11f18b97393',1,'FlangerDAFX::getModFreq()'],['../class_vibrato_d_a_f_x.html#a42cdca008e09fafb480a32ac089d400a',1,'VibratoDAFX::getModFreq()']]],
  ['getorder_74',['getOrder',['../class_filter_block.html#ac664dd0375228e3b8846698afb97afd3',1,'FilterBlock']]],
  ['getsamplingfactor_75',['getSamplingFactor',['../class_downsampling.html#aea83c56e57e9ed7c5bdf31b14e6ce1ed',1,'Downsampling']]],
  ['getstate_76',['getState',['../class_filter_block.html#ad2a4cb2c529f2837600677012473a596',1,'FilterBlock']]],
  ['getwidth_77',['getWidth',['../class_flanger_d_a_f_x.html#a745175706be56710722c8442b5743f39',1,'FlangerDAFX::getWidth()'],['../class_vibrato_d_a_f_x.html#a39e7717f091594a8b75a4132066e7826',1,'VibratoDAFX::getWidth()']]]
];
