/*
	Noise canceling using STFT and spectral subtraction
	--------------------------------------------------
	The Bela FFT library is used to create a simple spectral subtraction
	noise reduction algorithm.
	
	Analog IN 0 is used to control the threshold for when a frequency bin
	is removed. The upper and lower bound can be set by changing kThMAX and
	kThMIN respectivly.
	
	Using the 'AuxiliaryTask' Bela object, the FFT can be calculated at in a 
	lower priority thread than audio processing render() function, and hereby 
	minimize the risc of getting underruns.
	
	This project was inspired by the Bela phase vocoder example code.
	https://youtu.be/xGmRaTaBNZA
	
	Author: Nikolai Linden-Vørnle 2021
*/

#include <Bela.h>
#include <libraries/Fft/Fft.h>
#include <vector>
#include <cmath>
#include <libraries/OnePole/OnePole.h>

// Noise thresholds
float gNoiseThreshold_dB; 
const float kThMIN = -60.0;
const float kThMAX = -10.0;

// Analog input controls
OnePole gInputFilter;		// create a 1-pole filter for smoothing of analog input
const int kInputPot = 0;	// select what analog input should be used
int gPrintCounter;			// used to print the threshold to the terminal

// Blocksize for the calculation of the FFT
const int gBufferSize = 2048;	// at least Hopsize plus FFT size long [longer is also ok]
const int gFftSize = 512;		// FFT size should be a power of 2 for efficent FFT calculation
const int gHopsize = 128;		// when using a Hanning Window, Hop size should be 1/4 og FFT size

// Count used to keep track of #samples ellapsed
int gHopCounter;

// Buffers used to store the I/O blocks for the FFT processing and windowing
std::vector<float> gWindowBuffer;
std::vector<float> gInputBuffer;
std::vector<float> gOutputBuffer;

// The FFT object
Fft gFft;

// Pointers for accesing Buffers
int gInputBufferPointer;
int gOutputBufferReadPointer;
int gOutputBufferWritePointer;

// AuxiliaryTask object used to schedule the FFT calculation on the Bela.
AuxiliaryTask gFFT_Task;

// template function. This function is passed to the AuxiliaryTask
void process_fft_background(void*);

bool setup(BelaContext *context, void *userData)
{
	// Checks if the Bela is setup for stereo audio I/O
	if (context->audioInChannels != 2 && context->audioOutChannels != 2) {
		return false;
	}
	
	// initialize inpur smooth filter
	gInputFilter.setup(30.0f, context->audioSampleRate, OnePole::LOWPASS);
	
	// initialize print counter
	gPrintCounter = 0;
	
	// initialize noise canceling threshold
	gNoiseThreshold_dB = -30.0;
	
	// setup the FFT object
	gFft.setup(gFftSize);
	
	// initialize input/outout Buffers
	gInputBuffer.resize(gBufferSize, 0.0);
	gOutputBuffer.resize(gBufferSize, 0.0);
	
	// initialize Hanning windowing function
	gWindowBuffer.resize(gFftSize,0);
	for (int n = 0; n < gFftSize; n++) {
		gWindowBuffer[n] = 0.5 * (1 - cosf((2*M_PI / (float) (gFftSize-1)) * n));
	}
	
	// create an AuxiliaryTask for multi threading the FFT workload
	gFFT_Task = Bela_createAuxiliaryTask(&process_fft_background, 90, "fft_background_calculation");
	
	// initialize buffer Pointers and counters.
	gInputBufferPointer = 0;
	gOutputBufferReadPointer = 0;
	gOutputBufferWritePointer = gHopsize;	// The write pointer is offset one by one hopsize to ensure causality
	gHopCounter = 0;
	
	return true;
}

void process_fft (std::vector<float> const& inputBuffer_, int inputBufferPointer_, std::vector<float>& outputBuffer_, int outputBufferPointer_) {
	// static buffer to store the unwrapped block from the input buffer
	static std::vector<float> unwrappedBuffer(gFftSize);
	
	// extract one block from the input buffer
	for (unsigned int n = 0; n < gFftSize; n++) {
		unwrappedBuffer[n] = inputBuffer_[(inputBufferPointer_ + n - gFftSize + gBufferSize) % gBufferSize];
	}
	
	// multiply with window function
	for (unsigned int n = 0; n < gFftSize; n++) {
		unwrappedBuffer[n] = unwrappedBuffer[n] * gWindowBuffer[n];
	}
	
	// Perfom FFT in the unwrapped buffer
	gFft.fft(unwrappedBuffer);
	
	// Do the spectral subtraction!
	for (unsigned int n = 0; n < gFftSize; n++) {
		if (20.0 * log10 (gFft.fda(n)) <= gNoiseThreshold_dB) {
			gFft.fdr(n) = 0;
			gFft.fdi(n) = 0;
		}
	}
	
	// calculate the inverse FFT
	gFft.ifft();
	
	// calculate output samples and fill perform Overlap-Add in the output buffer
	for (unsigned int n = 0; n < gFftSize; n++) {
		int index = (outputBufferPointer_ + n) % gBufferSize;
		outputBuffer_[index] += gFft.td(n); 
	}
}

void process_fft_background(void*) {
	process_fft(gInputBuffer, gInputBufferPointer, gOutputBuffer, gOutputBufferWritePointer);
	// update output write pointer
	gOutputBufferWritePointer = (gOutputBufferWritePointer + gHopsize) % gBufferSize;
}

void render(BelaContext *context, void *userData)
{
	// Set the noise canceling threshold depending on the position of the input pot
	float smoothInput = gInputFilter.process(analogRead(context, 0, kInputPot));
	gNoiseThreshold_dB = map(smoothInput, 0.0, 3.3f/4.096f, kThMIN, kThMAX);
	
	// print the noise canceling threshold to the terminal
	if (gPrintCounter >= (int)context->audioSampleRate*2) {
		rt_printf("Current noise canceling threshold: %f\n", gNoiseThreshold_dB);
		gPrintCounter = 0;
	}
	gPrintCounter += context->audioFrames;
	
	for (unsigned int n = 0; n < context->audioFrames; n++) {
		// get the current input sampels and convert to mono
		float x = 0;
		for (unsigned int ch = 0; ch < context->audioInChannels; ch++) {
			x += audioRead(context, n, ch);
		}
		x = x / context->audioInChannels;
		
		// store current sample in input buffer
		gInputBuffer[gInputBufferPointer] = x;
		
		// increament pointer and wrap input buffer pointer
		gInputBufferPointer++;
		if (gInputBufferPointer >= gBufferSize) {
			gInputBufferPointer = 0;
		}
		
		// if hopsize amount of samples have been filled into the input buffer, schedule FFT/IFFT processing
		gHopCounter++;
		if (gHopCounter >= gHopsize) {
			// PERFORM FFT
			Bela_scheduleAuxiliaryTask(gFFT_Task);
			gHopCounter = 0;
		}
		
		// Write a sample from the outbut buffer to the Bela 
		for (unsigned int ch = 0; ch < context->audioInChannels; ch++) {
			audioWrite(context, n, ch, gOutputBuffer[gOutputBufferReadPointer]);
		}
		
		// reset sample in output buffer after being written to the audio output buffer
		gOutputBuffer[gOutputBufferReadPointer] = 0;
		
		// Increament and wrap output buffer read pointer
		gOutputBufferReadPointer++;
		if (gOutputBufferReadPointer >= gBufferSize) {
			gOutputBufferReadPointer = 0;
		}
	}
}

void cleanup(BelaContext *context, void *userData)
{
}