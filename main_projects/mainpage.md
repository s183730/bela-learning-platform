# Welcome to the documentation!

This is the documentation for the Bela DSP Box to be used in the DTU course 22051.

Most of the projects created have thier functionalities build as classes/objects.
This makes it easy to use the objects to create your own custom Bela projects.

The different classes and their functionalities can be found in the \c Classes tab.

The source code used to generate the documentation can be found in the \c Files tab.

To get started, watch the introduction video by clicking [HERE](../bela_introduction_final.mov)

Author: Nikolai Linden-Vørnle 2021

- - -

#### The Layout ####

The naming of the controls on the Bela DSP Box can be seen in the picture below.

To connect the hardware correctly to your own standalone Bela Platform, use the [pin-diagram](https://learn.bela.io/pin-diagram/) provided by Bela.

Connect your potentiometers and buttons shown bellow. The LEDs are connected to the <em>Analog OUT 0-2</em>.

<img src="../bela_prototype_text.jpg" alt="bela layout" width="500"/>

- - -

#### The Basis Project ####

The basis project is used to showcase the concepts of filtering and aliasing.

For more detail on the behaviour and different functions, take a look in @ref basisProject/render.cpp.

##### Hardware layout for the 'Basis Project' #####

|                  | Potentiometer ||| Buttons |||
|------------------|----------------------|----------------------|----------------------|-----------------------|----------------------|-----------------------|
| <b>Input</b>     | <em>Analog IN 0</em> | <em>Analog IN 1</em> | <em>Analog IN 2</em> | <em>Digital IN 0</em> | <em>Digital IN 1</em>| <em>Digital IN 2</em> |
| <b>Parameter</b> | Cutoff Frequency     | Q factor             | %Downsampling factor | Switch State          | Switch Order         | %Downsampling on/off  |

- - -

#### The Difference Equation project ####

This project can be used to do custom static filter designs by uploading filter coefficients to the Bela DSP Box.
The filter design can be done in a program like Matlab, and then the filter coeffifients can be uploaded to
the Bela DSP Box.

For more details, take a look in @ref differenceEquation/render.cpp.

- - -

#### The DAFX project ####

The DAFX project gives an example of how DSP can be used to create digital audio effects (DAFX).
The DAFX have three different effects, as stated in the hardware table below.

For more details on the behaviour, take a look in @ref dafxProject/render.cpp.

##### Hardware layout for the 'DAFX project' #####

The table below describes the hardware interface for the DAFX project.
When a button is pressed and held down, the potentiometers can change parameters on the different DAFX's.

| Effect Type        | Potentiometer ||| Assigned Button |
|--------------------|----------------------|----------------------|----------------------|-------------------------|
| ^                  | <em>Analog IN 0</em> | <em>Analog IN 1</em> | <em>Analog IN 2</em> | ^                       |
| <b>Delay</b>       | Mix                  | Delay time           | Feedback             | <em> Digital IN 2 </em> |
| <b>Flanger</b>     | Mix                  | Modulation Width     | Modulation frequency | <em> Digital IN 1 </em> |
| <b>Vibrato</b>     | ---                  | Modulation Width     | Modulation frequency | <em> Digital IN 0 </em> |

- - -

#### Spectral Subtraction project ####

The spectral subtration project uses FFT on the Bela Platform to perform noise canceling by spectral subtraction.

This is done by setting the magnitude of frequency bins below as certain threshold to 0.

<em>Analog IN 0</em> can be used to control the threshold for when the magnitude of the bins are set to 0.

For more deetails, take a look in @ref fftSpectralSubtraction/render.cpp.

- - -